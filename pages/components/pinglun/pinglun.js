// pages/components/pinglun/pinglun.js
Component({
  properties:{
    model:Object
  },
  /**
   * 页面的初始数据
   */
  data: {
  },
  methods:{
    myTreeToPinglunEvent(e) {
      // 这里就是子组件传过来的内容了
      this.triggerEvent('event',e.detail)
      // console.log("进入myTreeToPinglunEvent",e.detail)
    },
    pinglunToShequ(e){
      /*{
        comment_id:'',
        userid:'',
        parentId:'',
        content:'',
        answerId:'',
        username:''
      }*/
      const result = {
        parentId:e.currentTarget.dataset.parentid,
        parentName:e.currentTarget.dataset.parentname
      }
      this.triggerEvent('event',result)
      // console.log("进入pinglunToShequ")
    },
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})