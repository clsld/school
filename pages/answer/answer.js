//answer.js
var util = require('../../utils/util.js')
const { $Toast } = require('../../components/iview-weapp/base/index');
var app = getApp()
Page({
  data: {
    motto: '知乎--微信小程序版',
    userInfo: {},
    pinglun:"",
    result:{
    },
    commentShow: false,
    list:[
    //   {
    //   comment_id: 1,
    //   username: "陈霖",
    //   userid: "1",
    //   answer_id: 1,
    //   parent_id: null,
    //   parent_name: null,
    //   content: "挺不错的",
    //   avatar_url: null,
    //   children: [{
    //     comment_id: 2,
    //     username: "钟科杰",
    //     userid: "2",
    //     answer_id: 1,
    //     parent_id: 1,
    //     parent_name: "陈霖",
    //     content: "这有啥不错的，也就一般般吧！",
    //     avatar_url: null,
    //     children: [{
    //       comment_id: 3,
    //       username: "叶勇",
    //       userid: "3",
    //       answer_id: 1,
    //       parent_id: 2,
    //       parent_name: "钟科杰",
    //       content: "你懂啥？",
    //       avatar_url: null,
    //       children: [{
    //         comment_id: 4,
    //         username: "科年",
    //         userid: "4",
    //         answer_id: 1,
    //         parent_id: 3,
    //         parent_name: "叶勇",
    //         content: "傻逼叶勇",
    //         avatar_url: null,
    //         children: [{
    //           comment_id: 5,
    //           username: "叶勇",
    //           userid: "3",
    //           answer_id: 1,
    //           parent_id: 4,
    //           parent_name: "科年",
    //           content: "傻逼科年",
    //           avatar_url: null,
    //           children: null
    //         }]
    //       }]
    //     }, {
    //       comment_id: 6,
    //       username: "钟科杰",
    //       userid: "2",
    //       answer_id: 1,
    //       parent_id: 3,
    //       parent_name: "叶勇",
    //       content: "就你懂",
    //       avatar_url: null,
    //       children: null
    //     }]
    //   }]
    // },
    // {
    //   comment_id: 7,
    //   username: "宝哥",
    //   userid: "5",
    //   answer_id: 1,
    //   parent_id: null,
    //   parent_name: null,
    //   content: "辣鸡",
    //   avatar_url: null,
    //   children: null
    // },
    // {
    //   comment_id: 8,
    //   username: "日林",
    //   userid: "6",
    //   answer_id: 1,
    //   parent_id: null,
    //   parent_name: null,
    //   content: "挺不错的嘛！",
    //   avatar_url: null,
    //   children: null
    // },
  ],
  answer_id:"",
  answer:[],
  question_id:" ",
  question:[],
  parent_id:undefined,
  parent_name:undefined
  },
  //事件处理函数
  toQuestion: function() {
    wx.navigateTo({
      url: '../question/question'
    })
  },
  pinlun(){
    this.setData({
      commentShow:true,
      parent_id:undefined,
      parent_name:undefined
    })
  },
  fabuComment:function(){
    // result:{
    //   username: "",
    //   userid: "",
    //   answer_id: Number,
    //   parent_id: "",
    //   parent_name: "",
    //   content: "",
    //   avatar_url: "",
    // },
    console.log(app.globalData)
    console.log(this.parent_id)
    this.data.result.username = app.globalData.userInfo.nickName;
    this.data.result.userid = app.globalData.openid;
    this.data.result.answer_id = this.data.answer_id
    if(this.data.parent_id != undefined)
      this.data.result.parent_id = this.data.parent_id;
    else
      this.data.result.parent_id  = -1
    this.data.result.parent_name = this.data.parent_name;
    this.data.result.content = this.data.pinglun;
    this.data.result.avatar_url = app.globalData.userInfo.avatarUrl;
    console.log(this.data.result)
    var that = this
    wx.request({
      url: app.globalData.host+'/addComment',
      data: this.data.result,
      success(res){
        $Toast({
          content: '评论成功！',
          type: 'success'
      });
      that.setData({
          commentShow:false
        })
      },fail(err){
        $Toast({
          content: '评论失败！',
          type: 'warning'
      });
        console.log(err);
      }
    })
  },
  getTextValue(e){
    console.log(e.detail)
    this.setData({
      pinglun:e.detail.value
    })
  },
  onLoad: function (option){
    this.setData({
      answer_id:option.answer_id,
      question_id:option.question_id
    })
    var that = this
    console.log(this.data.answer_id)
    wx.request({
      url: app.globalData.host+'/getAnswerById',
      data:{
        answer_id:this.data.answer_id
      },
      success(res){
        that.setData({
          answer:res.data
        })
        console.log(that.data.answer)
      },
      fail(err){
        console.log(err)
      }
    })
    wx.request({
      url: app.globalData.host+'/selectQuestion',
      data:{
        question_id:this.data.question_id
      },
      success(res){
        that.setData({
          question:res.data
        })
      },
      fail(err){
        console.error(err)
      }
    })
    wx.request({
      url: app.globalData.host+'/getCommentByAnswerId',
      data:{
        answerId:this.data.answer_id
      },success(res){
        console.log(res)
         that.setData({
          list:res.data
        })
      },fail(err){
      }
    })
  },
  zangtong(){
    let that = this
    wx.request({
      url: app.globalData.host+'/addZang',
      data:{
        answer_id:this.data.answer_id
      },
      success(res){
        console.log(res.data)
        $Toast({
          content: '点赞成功！',
          type: 'success'
      });
        that.setData({
          "answer.zang":res.data
        })
      },
      fail(req){
        $Toast({
          content: '点赞失败！',
          type: 'warring'
      });
        console.error(err)
      }
    })
  },
  pinlunToShequEvent(e) {
    // 这里就是子组件传过来的内容了
      // console.log("进入pinlunToShequEvent")
      console.log(e.detail)
      this.setData({
        commentShow:true,
        parent_id:e.detail.parentId,
        parent_name:e.detail.parentName
      })
  },
})
