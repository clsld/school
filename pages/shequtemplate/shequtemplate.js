// pages/shequ/shequ.js
let _page;
Component({
  /**
   * 页面的初始数据
   */
  data: {
    commentShow:false,
    ellipsis: true,
    list:[{
      commentId:"1",
      name: "陈霖",
      text: "挺不错的",
      ppname: null,
      children: [{
        commentId:'4',
        text: '这有啥不错的，也就一般般吧',
        name: "钟科杰",
        ppname: "陈霖",
        children: [{
          commentId:'5',
          name: "叶勇",
          text: "你懂啥",
          ppname: "钟科杰",
          children: [{
            commentId:"6",
            name: "科年",
            text: "傻逼叶勇",
            ppname: "叶勇",
            children: [{
              commentId:'7',
              name: "叶勇",
              text: "傻逼叶勇",
              ppname: "科年",
              children: null
            }]
          }]
        }, {
          commentId:'8',
          name: "钟科杰",
          text: "就你懂？",
          ppname: "叶勇",
          children: null
        }]
      }]
    },
    {
      commentId:'2',
      name: "宝哥",
      text: "辣鸡",
      children: null
    },
    {
      commentId:'3',
      name: "日林",
      text: "挺不错的嘛",
      children: null
    },
  ],
  },
  methods:{
    pinlunToShequEvent(e) {
      // 这里就是子组件传过来的内容了
        // console.log("进入pinlunToShequEvent")
        console.log(e.detail)
        this.setData({
          commentShow:true
        })
    },
    ellipsis() {
      _page = this;
      let value = !this.data.ellipsis;
      _page.setData({
        ellipsis: value,
      });
    },
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // wx.request({
    //   url: "http://172.16.169.3:8000/aaa",
    //   success(e) {
    //     console.log(e.data);
    //     that.setData({
    //       list: e.data,
    //     });
    //     console.log(that.data.list);
    //   },
    //   fail() {
    //     console.log("失败");
    //   },
    // });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
});
